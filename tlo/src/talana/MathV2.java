package talana;

public class MathV2 {
    public static void main(String[] args) {

        double pi = 3.14159265;
        double result1 = 35 * 2 * (3 - 5) / (4 - 5); // Auf "Nummer sicher gehen" und als double deklarieren
        double result2 = 4 * 3 / 8.0;
        double result3 = 3 * (4 - (2 * 2)) / pi;
        double result4 = 2 * ((3 + 5) * (4 - 6) - 2) + 25;
        double result5 = 1 + 2 + 3 + (-5) + 0.5;

        System.out.println(result1); // 140.0
        System.out.println(result2); // 1.5
        System.out.println(result3); // 0.0
        System.out.println(result4); // -11.0
        System.out.println(result5); // 1.5

        // System.out.println(Math.PI);
        System.out.print(pi + " == ");
        System.out.println(StrictMath.PI);

    }
}
